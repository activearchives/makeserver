
HTMLFromMarkdown = Builder(action="""pandoc --css ${SOURCES[1]} --standalone ${SOURCES[0]} -o $TARGET""")
env = Environment(BUILDERS={
    'HTMLFromMarkdown': HTMLFromMarkdown
})

from pathlib import Path 

for path in Path(".").glob("*.md"):
    env.HTMLFromMarkdown(path.stem+".html", [str(path), "style.css"])

# this doesn't work since the files are served statically (can't bootstrap)
# cclabsrcs = []
# for path in Path("front/*.js"):
#     cclabsrcs.append(str(path))
# for path in Path("front/style/*.css"):
#     cclabsrcs.append(str(path))

# Command("makeserver/data/htdocs/cclab/cclab.js", cclabsrcs, "yarn build")
