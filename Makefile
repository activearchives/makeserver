# install:
# 	pip3 install -e .


mdsrc=$(shell ls *.md)
html_from_md=$(mdsrc:%.md=%.html)

all: $(html_from_md)

print-%:
	@echo '$*=$($*)'


%.html: %.md
	pandoc --toc --css style.css --standalone $< -o $@


install-statics:
	mkdir -p makeserver/data/htdocs/static/plyr
	cp node_modules/plyr/dist/plyr.css makeserver/data/htdocs/static/plyr/
	mkdir -p makeserver/data/htdocs/static/codemirror
	cp  node_modules/codemirror/lib/codemirror.css makeserver/data/htdocs/static/codemirror/