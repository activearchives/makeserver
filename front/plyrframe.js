import Plyr from 'plyr';
import * as timecode from './timecode.js';

function parseTime(h) {
  const m = h.match('t=([^&,]*)');
  // console.log("parseTime", h, m);
  if (m) { return timecode.timecodeToSeconds(m[1]); }
  return null;
}

// function stripFragment(url) {
//   const p = url.indexOf('#');
//   if (p >= 0) { return url.substring(0, p); }
//   return url;
// }

const pat = /^https?:\/\/(?:(?:www\.youtube\.com\/watch\?v=(.+))|(?:vimeo\.com\/(\d+)))$/;

export default class PlyrFrame {
  static sniff(url) {
    const m = pat.exec(url);
    return (m !== null);
  }

  constructor(frame, baseurl, fragment) {
    this.url = baseurl;
    // console.log("plyrframe.constructor");
    this.frame = frame;
    this.video = document.createElement('video');
    this.elt = this.video;
    this.video.setAttribute('autoplay', '');
    this.video.setAttribute('controls', '');
    this.frame.elt.appendChild(this.video);
    // console.log("adding load listeneer")
    // this.video.addEventListener("loaded", this.load.bind(this));
    // console.log('youtube: match!', m, this.contents);
    this.video = new Plyr(this.video);
    // var src =
    const m = pat.exec(this.url);
    const plyrSrc = m[1] ? { src: this.url, provider: 'youtube' } : { src: this.url, provider: 'vimeo' };
    // console.log('plyrSrc', plyrSrc);
    this.video.source = {
      type: 'video',
      sources: [plyrSrc],
    };
    this.video.on('timeupdate', this.timeupdate.bind(this));

    const t = parseTime(fragment);
    if (t) {
      this.video.currentTime = t;
      this.video.play();
    }
  }

  timeupdate() {
    this.frame.hashchange(this, this.fragment);
  }

  set fragment(f) {
    // console.log("plyrframe set fragment", f);
    const t = parseTime(f);
    if (t !== null) {
      this.video.currentTime = t;
      this.video.play();
    }
  }

  get fragment() {
    return `#t=${timecode.secondsToTimecode(this.video.currentTime)}`;
  }

  toggle() {
    if (this.video.paused) {
      this.video.play();
    } else {
      this.video.pause();
    }
  }

  dispose() {
    this.video.destroy();
  }
}
