export function stripFragment(url) {
  const p = url.indexOf('#');
  if (p >= 0) { return url.substring(0, p); }
  return url;
}

export function parseFragment(url) {
  // console.log("parseFragment", url);
  const ret = {};
  if (url.indexOf('#') >= 0) {
    const p = url.split('#', 2);
    [ret.base, ret.fragment] = p;
    // ret.fragment = p[1];
  } else {
    ret.base = url;
    ret.fragment = '';
  }
  return ret;
}
