import * as timecode from './timecode.js';

function parseTime(h) {
  const m = h.match('t=([^&,]*)');
  // console.log("parseTime", h, m);
  if (m) { return timecode.timecodeToSeconds(m[1]); }
  return null;
}

// function stripFragment(url) {
//   const p = url.indexOf('#');
//   if (p >= 0) { return url.substring(0, p); }
//   return url;
// }

const pat = /\.(mp4|webm|ogv|ogg|mp3)$/;

export default class MediaFrame {
  static sniff(url) {
    const m = pat.exec(url);
    return (m !== null);
  }

  constructor(frame, baseurl, fragment) {
    this.url = baseurl;
    // console.log("mediaframe.constructor");
    this.frame = frame;
    this.video = document.createElement('video');
    this.frame.elt.appendChild(this.video);
    this.video.setAttribute('autoplay', '');
    this.video.setAttribute('controls', '');
    this.video.addEventListener('timeupdate', this.timeupdate.bind(this));
    this.video.src = this.url;
    const t = parseTime(fragment);
    if (t) {
      this.video.currentTime = t;
      this.video.play();
    }
  }

  timeupdate() {
    this.frame.hashchange(this, this.fragment);
  }

  set fragment(f) {
    // console.log("mediaframe set fragment", f);
    const t = parseTime(f);
    if (t !== null) {
      this.video.currentTime = t;
      this.video.play();
    }
  }

  get fragment() {
    return `#t=${timecode.secondsToTimecode(this.video.currentTime)}`;
  }

  toggle() {
    if (this.video.paused) {
      this.video.play();
    } else {
      this.video.pause();
    }
  }

  dispose() {
    this.video.pause();
    this.video.remove();
  }
}
