/*
Goal:

CC manages targets + links

(editing is handled by cclab)
NB: hash vs. fragment
hash includes the leading hash tag, e.g. valid hash values are '', or '#foo'
while fragment never includes the hash, e.g. fragment is '', or 'foo'

*/

import { parseFragment } from './url.js';

export default class CCManager {
  // constructor() {}

  frameHashchange(frame, baseurl, fragment) {
    this.cclab.urlbar.url = baseurl;
    this.cclab.urlbar.fragment = fragment || '';
  }

  frameLoad(frame, url) {
    const { base, fragment } = parseFragment(url);
    this.cclab.urlbar.url = base;
    if (fragment) {
      this.cclab.urlbar.fragment = `#${fragment}`;
    }
  }

  frameClick(frame, d) {
    // console.log('cc doFrameClick', frame, d);
    // intercept clicks and potentially route them
    // returns true if handled
    // (originating client should evt call event.preventDefault)

    // let cclab intervene if present
    if (this.cclab) {
      if (this.cclab.frameClick(frame, d)) {
        return true;
      }
    }
    // translate target to frame ...
    if (this.frame && d.href) {
      this.frame.src = d.href;
      return true;
    }
    return false;
  }

  setActiveEditor(editor) {
    if (this.cclab) {
      this.cclab.setActiveEditor(editor);
    }
  }

  setActiveFrame(frame) {
    if (this.activeFrame !== frame) {
      this.activeFrame = frame;
    }
    if (this.cclab) {
      this.cclab.setActiveFrame(frame);
    }
  }
}
