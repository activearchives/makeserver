/* Fresh start 2021 */
import { each } from '@lumino/algorithm';
import { CommandRegistry } from '@lumino/commands';
// import { Message } from '@lumino/messaging';
import {
  BoxPanel, ContextMenu, SplitPanel, DockPanel, Menu, MenuBar, Widget,
} from '@lumino/widgets';
// CommandPalette
// import '@fortawesome/fontawesome-free/css/all.min.css';
import URLBar from './urlbar.js';
import EditorWidget from './editor.js';
import FrameWidget from './framewidget.js';
import Sniffer from './sniffer.js';
import CCManager from './cc.js';
// import { ListingWidget } from './listing.js';
// import * as reflinks from './reflinks.js';
import './style/index.css';
import * as reflinks from './reflinks.js';
import { parseFragment } from './url.js';

// Examples ?!
// https://github.com/jupyterlab/jupyterlab/tree/011ff58a61689b734e29a2fe2c97c9aaa52efe25/examples
// Commands
// so the "index" file here seems to be the outer layer to put all the pieces together..
// the filebrowser being a kind of "app" that is the connected to commands
// <!> https://github.com/jupyterlab/jupyterlab/blob/011ff58a61689b734e29a2fe2c97c9aaa52efe25/examples/filebrowser/src/index.ts
// import { FileBrowser, FilterFileBrowserModel } from '@jupyterlab/filebrowser';
// const fbModel = new FilterFileBrowserModel({
//   manager: docManager
// });
// const fbWidget = new FileBrowser({
//   id: 'filebrowser',
//   model: fbModel
// });

class CCLab {
  constructor() {
    // const dialog = document.getElementById('dialog');
    this.version = 'dev2021-02-17-17:46';

    const baseURL = new URL(window.location);
    baseURL.pathname = '';
    const cc = new CCManager();
    this.cc = cc;
    this.cc.cclab = this;
    this.cc.baseurl = baseURL.href;
    const dock = new DockPanel();
    this.dock = dock;
    dock.id = 'dock';

    this.initCommands();
    this.initCommandKeyBindings();
    this.initMenu();
    this.urlbar = new URLBar();
    this.urlbar.id = 'urlbar';
    cc.urlbar = this.urlbar;
    this.activeFrame = null;
    this.activeEditor = null;

    document.addEventListener('keydown', (event) => {
      // console.log("keydown", event);
      // if (event.key === 'Escape' && dialogIsOpen()) {
      //   closeDialog();
      // } else {
      this.commands.processKeydownEvent(event);
      // }
    });

    this.initContextMenu();

    const frame = new FrameWidget(cc, baseURL.href);
    cc.frame = frame;
    dock.addWidget(frame);

    const editor = new EditorWidget(cc, 'start.md');
    dock.addWidget(editor, { mode: 'split-right' }); // { mode: 'split-right' }
    // editor.setValue('<http://localhost:8000/>\n\n<http://vandal.ist/thesituationisttimes/video/2017-12-14/MVI_0033.web.mp4>\n\n<https://www.youtube.com/watch?v=162VzSzzoPs>\n\n');

    const sniffer = new Sniffer(); // VIDEO_URL
    dock.addWidget(sniffer, { mode: 'split-bottom', ref: editor });

    BoxPanel.setStretch(dock, 1);

    this.mainPanel = new SplitPanel({ direction: 'left-to-right', spacing: 0 });
    this.mainPanel.id = 'main';
    this.mainPanel.addWidget(dock);

    window.onresize = () => { this.mainPanel.update(); };

    Widget.attach(this.menubar, document.body);
    Widget.attach(this.urlbar, document.body);
    Widget.attach(this.mainPanel, document.body);
  }

  initCommands() {
    const commands = new CommandRegistry();
    this.commands = commands;

    commands.addCommand('file:edit', {
      label: 'Edit',
      iconClass: 'fa fa-edit',
      execute: () => {
        const editor = new EditorWidget(this.cc);
        this.dock.addWidget(editor);
      },
    });

    commands.addCommand('file:new', {
      label: 'New',
      mnemonic: 1,
      iconClass: 'fa fa-file',
      execute: () => {
        // console.log('Import...');
        // showOpenfile();
        const editor = new EditorWidget(this.cc);
        this.dock.addWidget(editor);
        // https://github.com/jupyterlab/jupyterlab/blob/011ff58a61689b734e29a2fe2c97c9aaa52efe25/examples/filebrowser/src/index.ts
        // execute: () => {
        //   each(fbWidget.selectedItems(), item => {
        //     docManager.openOrReveal(item.path);
        //   });
        // }
      },
    });

    commands.addCommand('file:save', {
      label: 'Save',
      mnemonic: 1,
      iconClass: 'fa fa-save',
      execute: () => {
        console.log('cclab save');
        const editor = this.activeEditor;
        if (editor) {
          fetch(editor.url, {
            method: 'POST',
            headers: {
              // 'Content-Type': 'application/json',
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            // body: {text: this.activeEditor.getValue()},
            body: `text=${encodeURIComponent(editor.getValue())}`,
          })
            .then((resp) => resp.json())
            .then((data) => {
              console.log('save resp', data);
              if (data.text === 'ok') {
                editor.doPostSave();
              }
            })
            .catch((error) => {
              console.log('save error', error);
            });
        }
        // showOpenfile();
        // https://github.com/jupyterlab/jupyterlab/blob/011ff58a61689b734e29a2fe2c97c9aaa52efe25/examples/filebrowser/src/index.ts
        // execute: () => {
        //   const context = docManager.contextForWidget(activeWidget);
        //   return context?.save();
        // }
      },
    });

    commands.addCommand('file:save-as', {
      label: 'Save As...',
      mnemonic: 1,
      iconClass: 'fa fa-save',
      execute: () => {
        // console.log('Save As...');
        // showOpenfile();
      },
    });

    commands.addCommand('media:toggle', {
      label: 'Play/Pause',
      mnemonic: 1,
      iconClass: 'fa fa-play',
      execute: () => {
        // console.log('Play/Pause media...');
        // showOpenfile();
        if (this.activeFrame) {
          this.activeFrame.toggle();
        }
      },
    });

    commands.addCommand('media:step-back', {
      label: 'Step back',
      mnemonic: 2,
      iconClass: 'fa fa-step-backward',
      execute: () => {
        // console.log('Media Back...');
        if (this.activeFrame) {
          this.activeFrame.jumpback();
        }
      },
    });

    commands.addCommand('media:step-forward', {
      label: 'Step forward',
      mnemonic: 3,
      iconClass: 'fa fa-step-forward',
      execute: () => {
        // console.log('Media Forward...');
        if (this.activeFrame) {
          this.activeFrame.jumpforward();
        }
      },
    });

    commands.addCommand('cc:pastefragment', {
      label: 'Paste fragment',
      mnemonic: 1,
      iconClass: 'fa fa-paste',
      execute: () => {
        // console.log('Paste fragment...');
        this.pasteLink();
      },
    });

    commands.addCommand('link:openInFrame', {
      label: 'Open in Frame',
      mnemonic: 1,
      iconClass: 'fa fa-file',
      execute: () => {
        console.log('Open in Frame');
        // showOpenfile();
        // const editor = new EditorWidget('', cc);
        // dock.addWidget(editor);
      },
    });

    commands.addCommand('link:openInEditor', {
      label: 'Open in Editor',
      mnemonic: 2,
      iconClass: 'fa fa-edit',
      execute: () => {
        console.log('Open in Editor');
        // showOpenfile();
        // const editor = new EditorWidget('', cc);
        // dock.addWidget(editor);
      },
    });

    commands.addCommand('link:hashlink', {
      label: 'Activate hash',
      mnemonic: 3,
      iconClass: 'fa fa-hashtag',
      execute: (event) => {
        console.log('hashlink', this, event);
        // showOpenfile();
        // const editor = new EditorWidget('', cc);
        // dock.addWidget(editor);
      },
    });
  }

  initCommandKeyBindings() {
    const { commands } = this;

    commands.addKeyBinding({
      keys: ['Accel E'],
      selector: 'body',
      command: 'file:edit',
    });

    commands.addKeyBinding({
      keys: ['Accel O'],
      selector: 'body',
      command: 'tic:new',
    });

    commands.addKeyBinding({
      keys: ['Accel S'],
      selector: 'body',
      command: 'file:save',
    });

    commands.addKeyBinding({
      keys: ['Accel Shift ArrowUp'],
      selector: 'body',
      command: 'media:toggle',
    });

    commands.addKeyBinding({
      keys: ['Accel Shift ArrowLeft'],
      selector: 'body',
      command: 'media:step-back',
    });

    commands.addKeyBinding({
      keys: ['Accel Shift ArrowRight'],
      selector: 'body',
      command: 'media:step-forward',
    });

    commands.addKeyBinding({
      keys: ['Accel Shift ArrowDown'],
      selector: 'body',
      command: 'cc:pastefragment',
    });
  }

  initMenu() {
    const { commands } = this;
    const filemenu = new Menu({ commands });
    this.filemenu = filemenu;
    filemenu.addItem({ command: 'file:edit' });
    filemenu.addItem({ command: 'file:new' });
    filemenu.addItem({ command: 'file:save' });
    filemenu.addItem({ command: 'file:save-as' });
    // filemenu.addItem({ type: 'separator' });

    filemenu.title.label = 'File';
    filemenu.title.mnemonic = 0;

    const mediamenu = new Menu({ commands });
    this.mediamenu = mediamenu;
    mediamenu.addItem({ command: 'media:toggle' });
    mediamenu.addItem({ command: 'media:step-back' });
    mediamenu.addItem({ command: 'media:step-forward' });
    mediamenu.addItem({ command: 'cc:pastefragment' });
    // filemenu.addItem({ type: 'separator' });

    mediamenu.title.label = 'Media';
    mediamenu.title.mnemonic = 0;

    this.menubar = new MenuBar();
    this.menubar.addMenu(this.filemenu);
    this.menubar.addMenu(this.mediamenu);
    this.menubar.id = 'menuBar';
  }

  initContextMenu() {
    const { commands } = this;
    const contextMenu = new ContextMenu({ commands });
    this.contextMenu = contextMenu;

    contextMenu.addItem({ command: 'link:hashlink', selector: '.cc-internal-link' });
    // contextMenu.addItem({ command: 'media:toggle', selector: '.cc-internal-link' });
    // contextMenu.addItem({ command: 'media:step-back', selector: '.cc-internal-link' });

    contextMenu.addItem({ command: 'media:toggle', selector: '.sniffer' });
    contextMenu.addItem({ command: 'media:step-back', selector: '.sniffer' });
    contextMenu.addItem({ command: 'media:step-forward', selector: '.sniffer' });
    contextMenu.addItem({ type: 'separator', selector: '.lm-CommandPalette-input' });

    document.addEventListener('contextmenu', (event) => {
      if (contextMenu.open(event)) {
        event.preventDefault();
      }
    });

    contextMenu.addItem({ command: 'link:openInFrame', selector: '.cclink' });
    contextMenu.addItem({ command: 'link:openInEditor', selector: '.cclink' });
  }

  tabForWidget(widget) {
    // shld be in lumino api, but I don't manage to find it..
    // so some code to search the dock for the tab matching a certain
    // widget
    each(this.dock.tabBars(), (tabbar) => {
      console.log('tabbar*', tabbar);
      each(tabbar.titles, (t) => {
        console.log('title', t);
      });
    });
  }

  openInEditor(href) {
    const editor = new EditorWidget(this.cc, href);
    this.dock.addWidget(editor);
  }

  frameClick(frame, { href, target }) {
    if (this.contextMenu.menu.isVisible) {
      this.contextMenu.menu.close();
    }
    // if (this.menubar.isVisible) {
    each(this.menubar.menus, (menu) => {
      if (menu.isVisible) {
        menu.close();
      }
    });
    // }
    if (href && href.match(/\.(md|py|css|js|json|txt)$/i)) {
      this.openInEditor(href);
      return true;
    }
    return false;
  }

  pasteLink() {
    let href = this.urlbar.url;
    const hash = this.urlbar.fragment;
    // console.log('pasteLink', href, hash);
    if (this.activeEditor) {
      const ev = this.activeEditor.getValue();
      const thereflinks = reflinks.extractReflinkDefinitions(ev);
      if (hash) {
        href = parseFragment(href).base + hash;
      }
      const compactHref = reflinks.compactRefLink(href, thereflinks);
      if (compactHref !== href) {
        this.activeEditor.replaceSelection(`[${compactHref}]`);
      } else {
        this.activeEditor.replaceSelection(`<${href}>`);
      }
    }
  }

  setActiveEditor(editor) {
    if (this.activeEditor !== editor) {
      this.activeEditor = editor;
    }
  }

  setActiveFrame(frame) {
    if (this.activeFrame !== frame) {
      this.activeFrame = frame;
    }
  }
}

window.onload = () => {
  // console.log("onload", CCLab);
  window.cclab = new CCLab();
};
