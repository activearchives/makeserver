import { Widget } from '@lumino/widgets';

export default class URLBar extends Widget {
  static buildLanguageSelector() {
    const ret = document.createElement('select');
    ['en', 'nl', 'fr'].forEach((x) => {
      const opt = document.createElement('option');
      opt.innerHTML = x;
      ret.appendChild(opt);
    });
    return ret;
  }

  static createNode() {
    const node = document.createElement('div');
    // let editor = document.createElement('div');

    const input = document.createElement('input');
    const fragment = document.createElement('input');
    const target = document.createElement('select');
    const lang = URLBar.buildLanguageSelector();
    const gobutton = document.createElement('button');
    gobutton.innerHTML = 'go';
    input.placeholder = 'URL';
    // content.appendChild(input);
    input.classList.add('url');
    fragment.classList.add('fragment');
    fragment.placeholder = '#';
    target.classList.add('target');
    node.appendChild(input);
    node.appendChild(fragment);
    node.appendChild(target);
    node.appendChild(lang);
    node.appendChild(gobutton);
    return node;
  }

  constructor() {
    // console.log("urlbar: name", name);
    super({ node: URLBar.createNode() });
    // let editor_div = this.node.getElementsByTagName('div')[0];
    // window.code = this.cm;
    this.setFlag(Widget.Flag.DisallowLayout);
    this.addClass('urlbar');
    // this.addClass(name.toLowerCase());
    // this.title.label = name;
    this.title.closable = false;
    // this.title.caption = `Editor: ${name}`;
  }

  // get editorDiv () {
  //   return this.node.getElementsByTagName('div')[0];
  // }

  get urlNode() {
    return this.node.querySelector('input.url');
  }

  get fragmentNode() {
    return this.node.querySelector('input.fragment');
  }

  set url(value) {
    this.urlNode.value = value;
  }

  get url() {
    return this.urlNode.value;
  }

  set fragment(value) {
    this.fragmentNode.value = value;
  }

  get fragment() {
    return this.fragmentNode.value;
  }

  setValue(text) {
    this.cm.setValue(text);
    // this.cm.update();
  }

  getValue() {
    return this.cm.getValue();
  }

  // onUpdateRequest (Message) {
  // }
  onAfterShow() {
    // console.log("editor.onAfterShow");
    this.cm.refresh();
  }

  onActivateRequest(Message) {
    if (this.isAttached) {
      this.inputNode.focus();
    }
  }
}
