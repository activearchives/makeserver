export function trimParentheses(m) {
  return m.replace(/^\((.+)\)$/, '$1');
}

// nb: trims []:? ... ie optional trailing colon too!
export function trimBrackets(m) {
  return m.replace(/^\[(.+)\]:?$/, '$1');
}

export function trimQuotes(m) {
  return (m || '').replace(/^["(']/, '').replace(/["(']$/, '');
}

export function extractReflinkDefinitions(text) {
  const ret = {};
  const pat = /^\[([^\]]+)\]:\s+([^ \n]+)(?: +("[^"]*"|'[^"]*'|\([^)]*\)|.*))?$/gm;
  text.replace(pat, (m, label, url, title) => {
    const mobj = { label, url, title: trimQuotes(title) };
    // console.log("extract reflink", mobj);
    ret[label.toLowerCase()] = mobj;
  });
  return ret;
}

export function xpandRefLink(href, reflinks) {
  let reflink;
  if (href.indexOf('#') >= 0) {
    const hrefp = href.split('#', 2);
    const base = hrefp[0];
    const fragment = hrefp[1];
    reflink = reflinks[base.toLowerCase()];
    if (reflink !== undefined) {
      // reflink DEFINED!!!!
      // console.log("REFLINK", href, reflink.url)
      return { href: `${reflink.url}#${fragment}`, title: reflink.title };
    }
  } else {
    // expand non hashes too ?!
    reflink = reflinks[href.toLowerCase()];
    if (reflink !== undefined) {
      return { href: reflink.url, title: reflink.title };
    }
    return null; // not sure what should be this case
    // return {href: href};
  }
  return null;
}

export function compactRefLink(href, reflinks) {
  // console.log("compactRefLink", href);
  let hrefb;
  let hsh = '';
  if (href.indexOf('#') >= 0) {
    [hrefb, hsh] = href.split('#', 2);
  } else {
    hrefb = href;
  }
  const labels = Object.keys(reflinks);
  for (let i = 0; i < labels.length; i += 1) {
    const label = labels[i];
    const v = reflinks[label];
    // console.log("checking", v.url, hrefb);
    if (v.url === hrefb) {
      // console.log("MATCH", v.label);
      return v.label + (hsh ? `#${hsh}` : '');
    }
  }
  return href;
}
