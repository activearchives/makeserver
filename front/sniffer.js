import { Widget } from '@lumino/widgets';

export default class Sniffer extends Widget {
  static createNode() {
    const node = document.createElement('div');
    return node;
  }

  constructor() {
    super({ node: Sniffer.createNode() });
    // let editor_div = this.node.getElementsByTagName('div')[0];
    // window.code = this.cm;
    this.setFlag(Widget.Flag.DisallowLayout);
    this.addClass('sniffer');
    this.title.label = 'Sniffer';
    this.title.closable = false;
    // this.title.caption = this.title.label;
  }
}
