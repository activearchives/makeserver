// import * as timecode from './timecode.js';
import { parseFragment } from './url.js';

/*

see this as a kind of "controller overlay"
patching link clicks to some kind of
"main / overarching instrument"
that decides what to do..
(and can change depending on)
want to avoid hard coding specific
behaviors into this code
at least: want to be able to
use in both EDITING context
and VIEWER.

*/

// function stripFragment(url) {
//   const p = url.indexOf('#');
//   if (p >= 0) { return url.substring(0, p); }
//   return url;
// }

export default class IFrame {
  static sniff(url) {
    return true;
  }

  constructor(frame, baseurl, fragment) {
    this.url = baseurl;
    // console.log("mediaframe.constructor");
    this.frame = frame;
    this.iframe = document.createElement('iframe');
    this.frame.elt.appendChild(this.iframe);
    // this.iframe.addEventListener('hashchange', this.hashchange.bind(this));
    this.iframe.addEventListener('load', this.load.bind(this));
    // console.log('iframe constructor', this.url, fragment);
    this.iframe.src = this.url + (fragment ? `#${fragment}` : '');
  }

  hashchange() {
    // console.log('iframe hashchange', this);
    this.frame.hashchange(this, this.iframe.contentWindow.location.hash);
  }

  load() {
    const newURL = this.iframe.contentWindow.location.toString();
    const { base } = parseFragment(newURL);
    this.url = base;
    this.frame.load(this, newURL);
    this.iframe.contentWindow.addEventListener('hashchange', this.hashchange.bind(this));
    this.iframe.contentDocument.addEventListener('click', this.clickhander.bind(this));
    this.mapLinks();
    // console.log('iframe load', this.iframe.contentWindow.location);
  }

  mapLinks() {
    const cdocument = this.iframe.contentDocument;
    const links = cdocument.querySelectorAll('a');
    for (let i = 0; i < links.length; i += 1) {
      const link = links[i];
      if (link.href.startsWith(`${this.url}#`)) {
        link.classList.add('cc-internal-link');
      } else {
        link.classList.add('cclink');
      }
    }
    if (this.frame.cc.contextMenu) {
      cdocument.addEventListener('contextmenu', (event) => {
        if (this.frame.cc.contextMenu.open(event)) {
          event.preventDefault();
        }
      });
      cdocument.addEventListener('mousedown', (event) => {
        if (this.frame.cc.contextMenu.menu.isVisible) {
          this.frame.cc.contextMenu.menu.close();
        }
      });
    }
    cdocument.addEventListener('click', (event) => {
      // console.log("click", event);
      if (this.frame.cc.frameClick(this, {
        href: event.target.href,
        target: event.target.target,
      })) {
        event.preventDefault();
      }
    });
    console.log(`added cclink to ${links.length} links`);
  }

  set fragment(f) {
    // console.log("iframe set fragment", f);
    this.iframe.contentWindow.location.hash = f;
    // when set on a click, this happens, but no hashchange event naturally occurs..
    this.frame.hashchange(this, f);
  }

  get fragment() {
    return this.iframe.contentWindow.location.hash;
  }

  clickhander(event) {
    const t = event.target;
    const tid = t.getAttribute('id');
    // console.log("iframe click", t, tid);
    if (tid) {
      this.frame.hashchange(this, `#${tid}`);
    }
  }

  dispose() {
    this.iframe.remove();
  }
}
