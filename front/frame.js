import * as timecode from './timecode.js';

import MediaFrame from './mediaframe.js';
import PlyrFrame from './plyrframe.js';
import IFrame from './iframe.js';
import { parseFragment } from './url.js';

const VIEWERS = [MediaFrame, PlyrFrame, IFrame];

/*

Frame is div to wrap an "abstract" iframe/media element

"fragment" aware: setting src with same base url + new fragment will
attempt to smoothly adjust display without reloading the resource.
Video and audio resources are displayed using video tag.
Plyr.js is used to display youtube + vimeo
Other content uses an iframe.

TODO implement generic jumpback/forward based on form of fragment
(if temporal do temporal jumping ... but just based on the fragment)

*/

/* eslint no-unused-vars: ["error", { "args": "none" }] */

function parseTime(h) {
  const m = h.match('t=([^&,]*)');
  // console.log("parseTime", h, m);
  if (m) { return timecode.timecodeToSeconds(m[1]); }
  return null;
}

export default class Frame {
  constructor(url, cc) {
    this.elt = document.createElement('div');
    this.frame = undefined;
    this.cc = cc;
    // this.url_base = undefined;

    // this.video = null;
    // this.iframe = null;
    // this.interval = null;
    // this.pageNumber = null;

    // this.setFlag(Widget.Flag.DisallowLayout);
    if (url) {
      this.src = url;
    }
  }

  setBaseURL(url, fragment) {
    // console.log('setBaseURL', url, fragment);
    for (let i = 0; i < VIEWERS.length; i += 1) {
      if (VIEWERS[i].sniff(url)) {
        if (this.frame) {
          this.frame.dispose();
          this.elt.innerHTML = '';
        }
        this.frame = new VIEWERS[i](this, url, fragment);
        break;
      }
    }
    // shouldn't get here
  }

  set src(url) {
    // console.log('frame set src', url);
    const purl = parseFragment(url);

    if (!this.frame || this.frame.url !== purl.base) {
      this.setBaseURL(purl.base, purl.fragment);
    } else {
      this.frame.fragment = purl.fragment;
    }

    //   this.iframe = document.createElement('iframe');
    //   this.contents.appendChild(this.iframe);
    //   this.iframe.src = url;
    //   this.iframe.addEventListener('load', this.load.bind(this));

    return this;
  }

  get src() {
    // console.log("frame get src");
    return this.frame.url;
  }

  set fragment(fragment) {
    if (this.frame) {
      this.frame.fragment = fragment;
    }
  }

  get fragment() {
    if (this.frame) {
      return this.frame.fragment;
    }
    return null;
  }

  hashchange(f, fragment) {
    this.cc.frameHashchange(f, f.url, fragment);
  }

  load(f, url) {
    this.cc.frameLoad(f, url);
  }

  toggle() {
    if (this.frame && this.frame.toggle) {
      this.frame.toggle();
    }
  }

  jumpforward() {
    if (this.frame) {
      if (this.frame.jumpforward) {
        this.frame.jumpforward();
      } else {
        const t = parseTime(this.frame.fragment);
        if (t !== null) {
          this.frame.fragment = `#t=${timecode.secondsToTimecode(t + 5)}`;
        }
      }
    }
  }

  jumpback() {
    if (this.frame) {
      if (this.frame.jumpback) {
        this.frame.jumpback();
      } else {
        const t = parseTime(this.frame.fragment);
        if (t !== null) {
          this.frame.fragment = `#t=${timecode.secondsToTimecode(Math.max(0, t - 5))}`;
        }
      }
    }
  }
}
