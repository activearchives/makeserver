import { Widget } from '@lumino/widgets';
import Frame from './frame.js';

/*
Lumino Widget wrapper for Frame
*/

export default class FrameWidget extends Widget {
  constructor(cc, url) {
    // console.log('FrameWidget', url);
    const node = document.createElement('div');
    super({ node });
    this.cc = cc;
    this.frame = new Frame(null, this.cc);
    this.contents = this.frame.elt;
    this.contents.classList.add('cc-frame-contents');
    const eyelid = document.createElement('div');
    eyelid.classList.add('eyelid');
    node.appendChild(this.contents);
    document.addEventListener('mousedown', () => {
      eyelid.classList.add('visible');
    }, true);
    document.addEventListener('mouseup', () => {
      eyelid.classList.remove('visible');
    }, true);
    node.appendChild(eyelid);

    this.addClass('cc-frame');
    // this.addClass(name.toLowerCase());
    this.title.label = 'Frame';
    this.title.closable = true;
    this.title.caption = `Frame: ${url}`;
    if (url) {
      this.frame.src = url;
    }
  }

  // onAfterShow() {
  //   console.log("editor.onAfterShow");
  //   this.cm.refresh();
  // }

  // onActivateRequest(Message) {
  //   if (this.isAttached) {
  //      this.inputNode.focus();
  //   }
  // }

  set src(value) {
    this.frame.src = value;
  }

  get src() {
    return this.frame.src;
  }

  // hashchange(f, fragment) {
  //   this.cc.hashchange(f.url, fragment);
  // }

  toggle() {
    if (this.frame && this.frame.toggle) {
      this.frame.toggle();
    }
  }

  jumpforward() {
    if (this.frame && this.frame.jumpforward) {
      this.frame.jumpforward();
    }
  }

  jumpback() {
    if (this.frame && this.frame.jumpback) {
      this.frame.jumpback();
    }
  }

  onAfterAttach() {
    // console.log("editor.onAfterAttach", this, "hidden", this.isHidden);
    if (!this.isHidden) {
      // initial state is this
      this.cc.setActiveFrame(this);
    }
  //   this.cm.refresh();
  }

  onAfterShow() {
    // console.log("editor.onAfterShow", this, this.isHidden);
    this.cc.setActiveFrame(this);
  //   this.cm.refresh();
  }
}
