import { Widget } from '@lumino/widgets';

import * as CodeMirror from 'codemirror';
import 'codemirror/mode/markdown/markdown.js';
// import 'codemirror/mode/lua/lua.js';
import 'codemirror/mode/javascript/javascript.js';
import 'codemirror/mode/python/python.js';
import 'codemirror/mode/htmlmixed/htmlmixed.js';
import 'codemirror/mode/css/css.js';
import 'codemirror/addon/fold/foldcode.js';
import 'codemirror/addon/fold/foldgutter.js';
// import 'codemirror/lib/codemirror.css';
// import 'codemirror/theme/monokai.css';
import * as reflinks from './reflinks.js';

/* eslint no-unused-vars: ["error", { "args": "none" }] */

function extractFilename(path) {
  const m = path.match(/[^/]*$/);
  if (m !== null) {
    return m[0];
  }
  return null;
}

function extractExtension(path) {
  const m = path.match(/[^.]*$/);
  if (m !== null) {
    return m[0];
  }
  return '';
}

const MODES_BY_EXT = {};
MODES_BY_EXT.md = { name: 'markdown', fold: true };
MODES_BY_EXT.py = { name: 'python' };
MODES_BY_EXT.html = { name: 'htmlmixed' };
MODES_BY_EXT.css = { name: 'css' };
MODES_BY_EXT.js = { name: 'javascript' };
MODES_BY_EXT.json = { name: 'javascript' };
// MODES_BY_EXT['makefile'] = { name: 'makefile' };

const CM_OPTS = {
  mode: MODES_BY_EXT.md,
  // mode: {'name': 'lua'},
  theme: 'default',
  lineNumbers: true,
  lineWrapping: true,
  // extraKeys: {"Ctrl-S": function(cm){ save(); }},
  foldGutter: true,
  gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
};

function cmMarkdownLinkElementOfInterest(elt) {
  // given a clicked element, return the "interesting" element if considered a markdown link
  // In a nutshell, always isolate the cm-url except in the case of
  // an implicit reference link (e.g. See [my website][]. )
  if (elt.classList.contains('cm-link')) {
    // check if followed by cm-url & use that (unless blank)
    if (elt.nextSibling
            && elt.nextSibling.classList
            && elt.nextSibling.classList.contains('cm-url')
            && elt.nextSibling.textContent !== '[]') {
      return elt.nextSibling;
    }
    return elt;
  } if (elt.classList.contains('cm-url')) {
    if (elt.textContent === '[]'
            && elt.previousSibling
            && elt.previousSibling.classList
            && elt.previousSibling.classList.contains('cm-link')) {
      return elt.previousSibling;
    }
    return elt;
  }
  return null;
}

export default class EditorWidget extends Widget {
  static createNode() {
    const node = document.createElement('div');
    const editor = document.createElement('div');

    // let input = document.createElement('input');
    // input.placeholder = 'Placeholder...';
    // content.appendChild(input);
    node.appendChild(editor);
    return node;
  }

  constructor(cc, url) {
    super({ node: EditorWidget.createNode() });
    // let editor_div = this.node.getElementsByTagName('div')[0];
    this.cm = null;
    this.cc = cc;

    this.editorDiv.addEventListener('click', this.click.bind(this));
    // window.code = this.cm;
    this.setFlag(Widget.Flag.DisallowLayout);
    this.addClass('content');
    // this.title.label = name || 'Untitled';
    this.title.closable = true;
    if (url) {
      const urlp = new URL(url, this.cc.baseurl);
      const filename = extractFilename(urlp.pathname);
      this.title.label = filename;
      this.ext = extractExtension(filename).toLowerCase();
      this.url = urlp.href;
      this.loadFromURL(this.url).then((text) => {
        this.initCodeMirror(text);
      });
    } else {
      this.title.label = 'Untitled.md';
      this.initCodeMirror();
    }
    // this.title.caption = this.title.label;
  }

  initCodeMirror(text) {
    const cmopts = { ...CM_OPTS };
    console.log('MODES_BY_EXT', this.ext, MODES_BY_EXT[this.ext]);
    cmopts.mode = MODES_BY_EXT[this.ext] || MODES_BY_EXT.md;
    if (text) {
      cmopts.value = text;
    }
    this.cm = CodeMirror(this.editorDiv, cmopts);
    this.cm.on('change', this.change.bind(this));
    const nop = function nop(cm) {};
    // block shift-ctrl-up to prevent shift-up + cc keys
    this.cm.setOption('extraKeys', {
      'Shift-Ctrl-Up': nop,
      'Shift-Ctrl-Down': nop,
      'Shift-Ctrl-Left': nop,
      'Shift-Ctrl-Right': nop,
    });
  }

  change() {
    this.title.className = 'unsaved';
  }

  async loadFromURL(url) {
    this.loadedfromURL = url; // just to use this
    const resp = await fetch(url);
    // apparently calling await in the return
    // of an async function is redundant
    // (it's implicit ?!)
    // return await resp.text();
    return resp.text();
  }

  doPostSave() {
    this.title.className = '';
  }

  get editorDiv() {
    return this.node.getElementsByTagName('div')[0];
  }

  get inputNode() {
    return this.node.getElementsByTagName('input')[0];
  }

  setValue(text) {
    this.cm.setValue(text);
    // this.cm.update();
  }

  getValue() {
    return this.cm.getValue();
  }

  replaceSelection(text) {
    this.cm.replaceSelection(text);
  }

  onResize() { // this fires initially
    // console.log("editor.onResize");
    if (this.cm) {
      this.cm.refresh();
    }
  }

  onAfterAttach() {
    // console.log("editor.onAfterAttach", this, "hidden", this.isHidden);
    if (!this.isHidden) {
      // initial state is this
      this.cc.setActiveEditor(this);
    }
  //   this.cm.refresh();
  }

  onAfterShow() {
    // console.log("editor.onAfterShow", this, this.isHidden);
    this.cc.setActiveEditor(this);
  //   this.cm.refresh();
  }

  // onAfterHide () {}
  // onUpdateRequest(Message) {}
  onActivateRequest(Message) {
    // console.log("editor.onActivateRequest", this, Message);
    if (this.isAttached && this.cm) {
      // this.inputNode.focus();
      this.cm.refresh();
      this.cm.focus();
    }
  }

  click(e) {
    let elt = e.target;
    let href; let
      target;
    // (src, classList)
    // console.log("_click", elt);
    elt = cmMarkdownLinkElementOfInterest(elt);
    if (elt) {
      let ref = elt.textContent;
      let m = ref.match(/\[(.+?)\]:?/);
      if (m) {
        ref = m[1].toLowerCase();
        // console.log("reflink", ref);
        const ev = this.getValue();
        const thereflinks = reflinks.extractReflinkDefinitions(ev);
        const reflink = reflinks.xpandRefLink(ref, thereflinks);
        if (reflink) {
          href = reflink.href;
          target = reflink.title;
        }
        // else { console.log('warning: undefined reference in link', ref); }
      } else {
        // trim ( ) and < > forms
        ref = ref.replace(/^\((.+)\)$/, '$1');
        ref = ref.replace(/^<(.+)>$/, '$1');
        // console.log("link", ref);
        m = ref.match(/^(.+?)(?: *(?:"(.+)")?)$/);
        [, href, target] = m;
        target = target || '';
      }
    }
    if (href) {
      // this.cc.handleLink(href, target);
      this.cc.frameClick(this, { href, target });
    }
  }
}
