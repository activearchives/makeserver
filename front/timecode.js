export const PATTERN = /(?:(\d\d):)?(\d\d):(\d\d)(?:[.,](\d{1,3}))?/;

export const formats = {
  html5: {
    alwaysFract: true,
  },
};

export function zeropadend(x, places) {
  let ret = x;
  while (ret.length < places) {
    ret += '0';
  }
  return ret;
}

export function secondsToTimecode(seconds, format = formats.html5) {
  // let use_format = format ? formats[format] : formats.html5;
  const hh = Math.floor(seconds / 3600);
  let s = seconds;
  let ret;
  s -= (hh * 3600);
  const mm = Math.floor(s / 60);
  s -= (mm * 60);
  const ss = Math.floor(s);
  const ff = s - ss;
  ret = `${((hh < 10) ? '0' : '') + hh}:`;
  ret += `${((mm < 10) ? '0' : '') + mm}:`;
  ret += ((ss < 10) ? '0' : '') + ss;
  if (ff > 0 || format.alwaysFract) {
    ret += `.${zeropadend(ff.toString().substring(2, 5), 3)}`;
  }
  return ret;
}

export function timecodeToSeconds(tc) {
  const m = tc.match(PATTERN);
  if (m) {
    return ((m[1] ? parseInt(m[1], 10) * 3600 : 0) + (parseInt(m[2], 10) * 60) + (parseInt(m[3], 10)) + (m[4] ? parseFloat(`0.${m[4]}`) : 0));
  }
  return null;
}

// module.exports = {
//   pattern: TC_PATTERN,
//   seconds_to_timecode,
//   timecode_to_seconds,
//   seconds_to_timecode,
// };
