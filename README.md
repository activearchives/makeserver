makeserver
=========


    in the course of a practice...  

	an EXPERIMENT becomes a RECIPE...  
	the gesture made AD HOC becomes a PATTERN...  
	we create new LANGUAGE to describe our work  
	we create new TOOLS to manipulate the material we work with  


makeserver is a python web server that incorporates the spirit of the *wiki* with a file-system oriented workflow drawing on the tools of software development. The server: (1) uses *make* to manage dynamic resources as *potential* transformations of files, triggering scripts *as needed* in the course of browsing, (2) uses *git* to enable navigable histories of sets of files. In essence, makeserver doesn't replace or claim to do more than what make or git do already, it just situates them the browser in a way that makes their operation more like that of a CMS or browsing the web. It acts as glue between javascript code and scripts by allowing authors to create web pages that refer to potential files that are then generated as needed according to the specific recipes of a makefile. In sum makeserver supports a paradigm of web development that's both tried and true (makefile and git-based workflows) and novel being (in part) driven by the webbrowser rather than exclusively the commandline.

By operating on (and producing) static files, makeserver operates as a kind of *scaffolding-server* whereby editors generate and shape resources by browsing them via the makeserver development server. In the process this produces archival results viewable (in parallel) via a traditional static web server. In this way the project is a *framework* for making [static site generators](https://www.staticgen.com/).  SSGs provide an alternative to CGI / LAMP / or other dynamic web publishing paradigms where server side code is implicated in all resource views. It is a *framework* in that rather than being defined for a specific purpose (blog posts, photo gallery, etc), the actual form of the website generated is determined by the contents of the itself malleable makefile and the scripts employed therein. In this way makeserver, though itself implemented in python, is language and tool agnostic (like a makefile).

The server is designed to be used both (1) *small scale* ad-hoc mode, running as needed over on a local machine, offline, for an individual or small group on a local network, and (2) online as a parallel server for editors.


![](16vmpw.jpg)


Some Design Goals
-------------------

* Promiscuity: Work in parallel with the commandline and other tools (rather than replacing them)
* Static by default: Generate resources that are "archival" in the sense of being -- in static form -- stable, add dynamic / external dependecies via scripts in a way that gracefully fails when these services are (no longer) available. 
* Discoverability: Be a framework that makes helps non-programmers and novice programmers work comfortably with tools traditionally available through the commandline
* Bootstrap other web development processes: By using makefiles (and starting from some other tried and true recipes), the goal is to start projects quickly. As the project develops, scripts can be iteratively tuned and (re)developed to match. Eventually, projects may well best be deployed with other forms of online services.
* Mixability: Embody a laterally composed system (as opposed to a monolithic/silo style service) Besides use via the built in server, components of makeserver are designed to be usable separately, mixed and matched as needed, and deployed as cgi+javascripts.


FAQ
----------------------

Q: Oh, so it's yet-another-markdown-based-static-site-generator. 
A: Well, it could be used to do that, but actually it depends on the rules you put in your makefile.

Q: Makefile, yeeeeeck. That's like from the 70s!  
A: Indeed, make was first released by Stuart Feldman in 1977. In a world where software platforms come and go with alarming frequency, it's interesting to look at tools that somehow manage to persist for decades. The version of make used by makeserver is [GNU Make](https://www.gnu.org/software/make/), that's at the heart of any modern GNU+Linux system.

Q: Oh, so it's an editor?  
A: Not exactly, though it does incorporate the ace.js project to provide browser-based file editing and viewing with syntax coloring.


Installation
-------------
TODO: Update for latest version.


Development
-----------------
To build from scratch

The frontend:

  yarn build


Usage
-----------

* Add ?remake to url to force a remake (equivalent to make -f)

Changelog
================
[See LOG](LOG.md) ([html](LOG.html))

